<?php
$size = 9;
$from = [3, 4];
$to = [8, 7];
$xArr = range(0, $size);
$yArr = range(0, $size);
$field = [];
foreach ($xArr as $x) {
    foreach ($yArr as $y) {
        $field[$x][] = $y;
    }
}
$neighboursInt = [];

$grid = [
    [['0,0'], ['0,1'], ['0,2'], ['0,3'], ['0,4'], ['0,5'], ['0,6'], ['0,7'], ['0,8'], ['0,9']],
    [['1,0'], ['1,1'], ['1,2'], ['1,3'], ['1,4'], ['1,5'], ['1,6'], ['1,7'], ['1,8'], ['1,9']],
    [['2,0'], ['2,1'], ['2,2'], ['2,3'], ['2,4'], ['2,5'], ['2,6'], ['2,7'], ['2,8'], ['2,9']],
    [['3,0'], ['3,1'], ['3,2'], ['3,3'], ['3,4'], ['3,5'], ['3,6'], ['3,7'], ['3,8'], ['3,9']],
    [['4,0'], ['4,1'], ['4,2'], ['4,3'], ['4,4'], ['4,5'], ['4,6'], ['4,7'], ['4,8'], ['4,9']],
    [['5,0'], ['5,1'], ['5,2'], ['5,3'], ['5,4'], ['5,5'], ['5,6'], ['5,7'], ['5,8'], ['5,9']],
    [['6,0'], ['6,1'], ['6,2'], ['6,3'], ['6,4'], ['6,5'], ['6,6'], ['6,7'], ['6,8'], ['6,9']],
    [['7,0'], ['7,1'], ['7,2'], ['7,3'], ['7,4'], ['7,5'], ['7,6'], ['7,7'], ['7,8'], ['7,9']],
    [['8,0'], ['8,1'], ['8,2'], ['8,3'], ['8,4'], ['8,5'], ['8,6'], ['8,7'], ['8,8'], ['8,9']],
    [['9,0'], ['9,1'], ['9,2'], ['9,3'], ['9,4'], ['9,5'], ['9,6'], ['9,7'], ['9,8'], ['9,9']],
];
function neighbours(array $address) {
    //    var_export($address);
    $values = [];
    $x = $address[0] - 1;
    $y = $address[1] - 1;
    for ($x; $x <= $address[0] + 1; $x++) {
        for ($y; $y <= $address[1] + 1; $y++) {
            $values[] = [$x, $y];
        }
    }

    return $values;
}

$neighbours = neighbours($from);
print_r($neighbours);
//die;
// FE
foreach ($field as $x => $row) {
    foreach ($row as $y) {
        $styleAppendix = '';
        //        if (in_array([$x, $y], $neighbours)) {
        //            $styleAppendix = ' background-color: #A0A0FF; ';
        //        }
        if ($from === [$x, $y]) {
            $styleAppendix = ' background-color: cyan; ';
        }
        if ($to === [$x, $y]) {
            $styleAppendix = ' background-color: yellow; ';
        }
        echo '
        <div
            style="width: 50px; height: 50px; border: 1px solid pink; float: left; ' . $styleAppendix . '"
            data-x="' . $x . '"
            data-y="' . $y . '">(' . $x . ', ' . $y . ')</div>';
    }
    echo '<br style="clear: both" />';
}