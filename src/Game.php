<?php

class Game {

    protected $positions = [];
    public $players = [];

    public function __construct($players) {
        $this->positions = $this->setPositions();
        $this->players = $players;
    }


    public function isFinished() {
        foreach ($this->players AS $player) {
            if($player->isActive === true) {
                return false;
            }
        }

        return true;
    }

    public function rollDice() {
        return mt_rand(1, 6);
    }

    public function move($position) {

    }


    protected function setPositions() {
        $positions = [];
        $i = 1;
        for ($i; $i <= 56; $i++) {
            $positions[$i] = [];
        }

        return $positions;
    }

    public function show() {
        foreach ($this->positions AS $key => $element) {
            echo '<div style="width: 60px; height: 60px; float: left; background-color: coral; margin: 0 1px 1px 0;">';
            echo $key;

            foreach ($this->players AS $player) {
                foreach($player->pawns AS $pawnKey => $pawn) {

                    if($pawn->currentPosition === $key) {
                        echo '<span style="color:' . $player->color . '">[' . ($pawnKey + 1) . ']</span>';
                    }
                }
            }

            echo '</div>';
        }
        echo '<br style="clear: both;">';
    }

    public function randomizer() {
        return (bool) mt_rand(0, 1);
    }
}