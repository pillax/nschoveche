<?php
/**
 * Created by PhpStorm.
 * User: i.porozhanov
 * Date: 14.9.2017 г.
 * Time: 16:33
 */

class Pawn {
    /** Possible pawn location */
    const IN_POOL = 1;
    const IN_REAL_PLAY = 2;
    const IN_PROTECTED_PLAY = 3;
    const IN_FINAL = 4;

    /** @param int Start position on map, depends on player color */
    private $startingPosition;

    /** @var int */
    public $turnsToGo = 62;

    /** @var int */
    public $currentPosition = 0;





    protected $inPlay = false;
    protected $finished = false;
    protected $inFinalRun = false;
    public $finalRunPosition = 1;


    public function __construct($startingPosition) {
        $this->startingPosition = $startingPosition;
    }

    /**
     * Set where pawn is located
     * 56 + 5 + 1
     *
     * @return int
     */
    public function getIn() {
        if($this->currentPosition === 0) {
            return self::IN_POOL;
        } elseif($this->currentPosition >= 1 && $this->currentPosition <= 56) {
            return self::IN_REAL_PLAY;
        } elseif($this->currentPosition >= 57 && $this->currentPosition <= 61) {
            return self::IN_PROTECTED_PLAY;
        } else {
            return self::IN_FINAL;
        }
    }

    /**
     * Add $positions to $this->currentPosition
     *
     * @param int $positions
     */
    public function move($positions) {
        $this->currentPosition += $positions;

        if($this->currentPosition >= $this->turnsToGo) {
            $this->finished = true;
        }
    }

    public function isInPlay() {
        return $this->inPlay;
    }

    public function isFinished() {
        return $this->finished;
    }

    public function enterGame() {
        $this->currentPosition = $this->startingPosition;
        $this->inPlay = true;
    }












    // TODO: possible moves

//
//    public function isInFinalRun() {
//        return $this->inFinalRun;
//    }
//

//
//    public function _move($positions) {
//
//
//        if($this->isInFinalRun()){
//
//        }
//
//        if($positions > $this->turnsToGo){
//            //go to final run and calculate
//            //final run position = positions - turns to go
//        }
//
//        $nextPosition = $this->currentPosition + $positions;
//
//        $this->turnsToGo -= $positions;
//        if($this->turnsToGo <= 0) {
//            $this->finished = true;
//            $this->inPlay = false;
//        }
//
//
//        $this->goToNextPosition($nextPosition);
//    }
//
//    private function goToNextPosition($nextPosition) {
//        if($nextPosition > 56){
//            $nextPosition-=56;
//        }
//
//        $this->currentPosition = $nextPosition;
//    }


}