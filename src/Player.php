<?php

class Player {
    public $color;
    public $startingPosition;
    public $endingPosition;
    public $pawns = [];
    public $isActive = true;

    public function __construct($color) {
        $this->color = $color;

        switch ($this->color){
            case 'green':
                $this->startingPosition = 1;
                $this->endingPosition = 56;
                break;
            case 'yellow':
                $this->startingPosition = 15;
                $this->endingPosition = 14;
                break;
            case 'red':
                $this->startingPosition = 29;
                $this->endingPosition = 28;
                break;
            case 'blue':
                $this->startingPosition = 43;
                $this->endingPosition = 42;
                break;
        }

        $pawn = new Pawn($this->startingPosition);
        $this->pawns = [
            $pawn,
            clone $pawn,
            clone $pawn,
            clone $pawn,
        ];
    }

    private function removeFinishedPawns() {
        foreach ($this->pawns as $key => $pawn) {
            if($pawn->isFinished()) {
                unset($this->pawns[$key]);
            }
        }
    }

    public function getPawnsInHouse() {
        $out = [];
        foreach ($this->pawns as $key => $pawn) {
            if( ! $pawn->isInPlay()) {
                $out[] = $pawn;
            }
        }
        return $out;
    }

    public function hasPawnInPlay() {
        $out = false;
        foreach ($this->pawns as $key => $pawn) {
            if($pawn->isInPlay()) {
                $out = true;
                break;
            }
        }
        return $out;
    }

    public function getPawnInPlay() {
        $this->removeFinishedPawns();

        $pawns = $this->pawns;

        while( ! empty($pawns)) {
            $key = array_rand($pawns);

            /** @var $pawn Pawn */
            $pawn = $pawns[$key];

            if( $pawn->isInPlay() && ! $pawn->isFinished()) {
                return $pawn;
            }

            unset($pawns[$key]);
        }

        if( ! $this->getPawnsInHouse()) {
            $this->isActive = false;
        }

        return false;
    }





}