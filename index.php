<?php
require_once __DIR__ . '/src/Game.php';
require_once __DIR__ . '/src/Player.php';
require_once __DIR__ . '/src/Pawn.php';

$game = new Game([
    new Player('green'),
    new Player('red'),
    new Player('yellow'),
    new Player('blue'),
]);


$i=0;
while ( ! $game->isFinished()) {
    foreach ($game->players AS $key => $player) {
        $skipMove = false;

        if(!$player->isActive) {
            unset($game->players[$key]);
            break;
        }

        $i++;
        if($i === 400) {
            echo '<p>Terminated</p>';
            break 2;
        }

        if( ! $player->isActive) {
            $skipMove = true;
        }

        $positions = $game->rollDice();

        $pawnInHouse = $player->getPawnsInHouse();

        // TODO randomizer - always true when all other pawns are finished or unmovable
        if($positions === 6 && $pawnInHouse && ($game->randomizer() || count($pawnInHouse) === 4)) {
            $pawnInHouse[array_rand($pawnInHouse)]->enterGame();
            $positions = 0;
        }

        // No pawn in game - skip
        if( ! $player->hasPawnInPlay()) {
            // skip move
            $skipMove = true;
        }

        $pawn = $player->getPawnInPlay();
        if($pawn) {
            $pawn->move($positions);
        }

        if( ! $skipMove) {
            $game->show();
        } else {
            echo '<b>Skipped</b>';
        }

        $pos = $pawn ? $pawn->currentPosition : 'unknown';

        echo '<p>color: ' . $player->color . '; dice: ' . $positions . '; pawInHouse: ' . count($pawnInHouse) . ' pawn pos: ' . $pos .  ' </p>';
    }

    echo str_repeat('-', 160) . '<br>';
}









function d($var) {
    echo '<div style="background-color: aqua">';
    var_dump($var);
    echo '</div>';
}
function dd($var) {
    d($var); die;
}
